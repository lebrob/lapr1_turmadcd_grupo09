import java.io.*;

public class teste {
    public static void main(String[] args){
    }

    public static boolean teste_encontra_criarArrayNumElementos(int dimensao, double[][] expectado) {
        double[][] resultado = projeto.criarArrayNumElementos(dimensao);
        for (int i = 0; i < expectado.length; i++) {
            if (expectado[i][0] != resultado[i][0])
                return false;
        }
        return true;
    }

    public static boolean teste_encontra_criarDeMatrizLeslie(int dimensao, double[][] expectado) {
        double[][] resultado = projeto.criarMatrizLeslie(dimensao);
        for (int i = 0; i < expectado.length; i++) {
            for (int j = 0; j < expectado.length; j++) {
                if (expectado[i][j] != resultado[i][j])
                    return false;
            }
        }
        return true;
    }
    public static boolean teste_encontra_criarArrayNumElementosFile(String nome, double[][] expectado, int dim)
            throws FileNotFoundException {
        double[][] resultado = projeto.criarArrayNumElementosFile(nome, dim);
        for (int i = 0; i < dim; i++) {
            if (expectado[i][0] != resultado[i][0])
                return false;
        }
        return true;
    }

    public static boolean teste_encontra_criarMatrizDeLeslieFile(int dimensao, double[][] expectado, String nome)
            throws FileNotFoundException {
        double[][] resultado = projeto.criarMatrizLeslieFile(nome, dimensao);
        for (int i = 0; i < expectado.length; i++) {
            for (int j = 0; j < expectado.length; j++) {
                if (expectado[i][j] != resultado[i][j])
                    return false;
            }
        }
        return true;
    }

    //----------------------------------------------------PONTO2--------------------------------------------------------
    public static boolean teste_encontra_multiplicarMatrizMembroAMembro(double[][] primeiraMatriz,
                                                                        double[][] segundaMatriz, int linha,
                                                                        int coluna, double expectado) {
        double resultado = projeto.multiplicarMatrizMembroAMembro(primeiraMatriz, segundaMatriz, linha, coluna);
        return expectado == resultado;
    }

    public static boolean teste_encontra_multiplicarMatrizes(double[][] primeiraMatriz, double[][] segundaMatriz,
                                                             double[][] expectado) {
        double[][] multiplicarMatrizMembroAMembro = projeto.multiplicarMatrizes(primeiraMatriz, segundaMatriz);
        for (int i = 0; i < multiplicarMatrizMembroAMembro.length; i++) {
            for (int u = 0; u < multiplicarMatrizMembroAMembro.length; u++) {
                if (multiplicarMatrizMembroAMembro[i][u] != expectado[i][u]) {
                    return false;
                }
            }
        }
        return true;
    }
    //----------------------------------------------------PONTO3--------------------------------------------------------
    public static boolean teste_encontra_dimensaoPopulacao(double[][] distribuicao, int dimensao,
                                                           double expectado) {
        double dimensaoPopulacao = projeto.evolucaoPopulacao(distribuicao, dimensao);
        return dimensaoPopulacao == expectado;
    }

    public static boolean teste_encontra_taxaVariacao(double[][] distribuicao, double[][] matrizLeslie, int dim,
                                                      double expectado) {
        double taxaVariacao = projeto.taxaVariacao(distribuicao, matrizLeslie, dim);
        return taxaVariacao == expectado;
    }

    public static boolean teste_encontra_matrizIndentidade(int dimensao, double[][] expectado) {
        double[][] matrizIdentidade = projeto.matrizIdentidade(dimensao);
        for (int i = 0; i < matrizIdentidade.length; i++) {
            for (int u = 0; u < matrizIdentidade.length; u++) {
                if (matrizIdentidade[i][u] != expectado[i][u]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean teste_encontra_distribuicaoDaPopulacao(double[][] matrizLeslie, double[][] nElementos,
                                                                 int gera, int dimensao, double[][] expectado) {
        double[][] distribuicaoDaPopulacao = projeto.distribuicaoDaPopulacao(matrizLeslie, nElementos, gera, dimensao);
        for (int i = 0; i < distribuicaoDaPopulacao.length; i++) {
            for (int u = 0; u < distribuicaoDaPopulacao.length; u++) {
                if (distribuicaoDaPopulacao[i][u] != expectado[i][u]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean teste_encontra_distribuicaoNormalizadaDaPopulacao(double[][] distribuicao, int dimensao
            , double[][] expectado) {
        double[][] distribuicaoNormalizadaDaPopulacao = projeto.distribuicaoNormalizadaDaPopulacao(distribuicao, dimensao);
        for (int i = 0; i < distribuicaoNormalizadaDaPopulacao.length; i++) {
            for (int u = 0; u < distribuicaoNormalizadaDaPopulacao.length; u++) {
                if (distribuicaoNormalizadaDaPopulacao[i][u] != expectado[i][u]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean teste_encontra_maximoValorProprio(double[][] matrizlelie, double expectado) {
        double maximoValorPro = projeto.maximoValorProprio(matrizlelie);
        return expectado == maximoValorPro;
    }

    public static boolean teste_encontra_vetorProprio(int dimensao, double maxValor, double[][] matrizleslie,
                                                      double[] expectado) {
        double[] vetorProprio = projeto.vetorProprio(dimensao, maxValor, matrizleslie);
        for (int i = 0; i < vetorProprio.length; i++) {
            if (vetorProprio[i] != expectado[i]) {
                return false;
            }
        }
        return true;
    }
}