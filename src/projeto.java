import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class projeto {
    static Scanner in = new Scanner(System.in);
    //Interação com o cliente/Outputs
    public static void main(String[] args) throws IOException {
        String sair;
        if (args.length == 0) {//Modo interativo sem ficheiro
            do{
                metodoInterativoSemFicheiro(args);
                System.out.println("-> Pretende encerrar o programa?");
                sair = in.next();
            }while(sair.equalsIgnoreCase("N") || sair.equalsIgnoreCase("No") ||
                    sair.equalsIgnoreCase("Nao") || sair.equalsIgnoreCase("Não"));
        } else if (args.length == 2) {//Modo interativo com ficheiro
            do{
                metodoInterativoComFicheiro(args);
                System.out.println("-> Pretende encerrar o programa?");
                sair = in.next();
            }while(sair.equalsIgnoreCase("N") || sair.equalsIgnoreCase("No") ||
                    sair.equalsIgnoreCase("Nao") || sair.equalsIgnoreCase("Não"));
        } else if (args.length > 2) {//Modo não interativo com ficheiro
            metodoNaoInterativo(args);
        }
    }

    public static void metodoInterativoSemFicheiro(String[] args) throws IOException {
        double[][] matrizLeslie, nElementos;
        int geracao, dimensao;
        System.out.println("Seja bem vindo, você encontra-se neste momento no modo interativo (sem ficheiro)");
        System.out.println("-> Número de faixas etárias?");
        dimensao = in.nextInt();
        System.out.println("-> Quantas gerações deseja calcular?");
        geracao = in.nextInt();
        while (dimensao <=0 || dimensao>200) {
            System.out.println("-> Introduza um valor possível para o número de faixas etárias.");
            System.out.println("-> Número de faixas etárias?");
            dimensao = in.nextInt();
        }
        while (geracao < 0) {
            System.out.println("-> Introduza um valor possível para o número de gerações.");
            System.out.println("-> Quantas gerações deseja calcular?");
            geracao = in.nextInt();
        }
        nElementos = criarArrayNumElementos(dimensao);
        if(nElementos[0][0] != -2){
            matrizLeslie = criarMatrizLeslie(dimensao);
            if(matrizLeslie[0][0] != -2) {
                //OUTPUT
                //2 && 3
                distribuicaoDaPopulacao(matrizLeslie, nElementos, geracao, dimensao);
                //4
                outputInterativo(matrizLeslie, nElementos, geracao, dimensao, args);
                System.out.println("---------------------------------------------------------------------------------");
                graficosInterativo();
            }
            else{
                args = new String[2];
                System.out.println("-> Qual é o ficheiro que pretende introduzir?");
                args[0] = "-n";
                args[1] = in.next();
                metodoInterativoComFicheiro(args);
            }
        }
        else{
            args = new String[2];
            System.out.println("-> Qual é o ficheiro que pretende introduzir?");
            args[0] = "-n";
            args[1] = in.next();
            metodoInterativoComFicheiro(args);
        }
    }

    public static void metodoInterativoComFicheiro(String[] args) throws IOException{
        double[][] matrizLeslie, nElementos;
        int geracao, dimensao;
        System.out.println("Seja bem vindo, você encontra-se neste momento no modo interativo (com ficheiro)");
        System.out.println("-> Quantas gerações deseja calcular?");
        geracao = in.nextInt();
        dimensao = dimensao(args[1]);
        if (dimensao > 0 && dimensao<200) {
            while (geracao < 0) {
                System.out.println("-> Introduza um valor possível para o número de gerações.");
                System.out.println("-> Quantas gerações deseja calcular?");
                geracao = in.nextInt();
            }
            nElementos = criarArrayNumElementosFile(args[1], dimensao);
            matrizLeslie = criarMatrizLeslieFile(args[1], dimensao);
            //OUTPUT
            //2 && 3
            distribuicaoDaPopulacao(matrizLeslie, nElementos, geracao, dimensao);
            //4
            outputInterativo(matrizLeslie, nElementos, geracao, dimensao, args);
            graficosInterativo();
            System.out.println("-------------------------------------------------------------------------------------");
        } else {
            System.out.println("-> Introduza um valor possível para o número de faixas etárias no seu ficheiro.");
            System.exit(0);
        }
    }

    public static void metodoNaoInterativo(String[] args) throws IOException{
        double[][] matrizLeslie, nElementos;
        int geracao, dimensao;
        String[] ficheiros = new String[2];
        geracao = geracoesNI(args);
        int cont = 0;
        for (String argumento : args) {
            if (argumento.endsWith(".txt")) {
                ficheiros[cont] = argumento;
                cont++;
            }
        }
        dimensao = dimensao(ficheiros[0]);
        if (dimensao > 0 && geracao >= 0 && dimensao<200) {
            nElementos = criarArrayNumElementosFile(ficheiros[0], dimensao);
            matrizLeslie = criarMatrizLeslieFile(ficheiros[0], dimensao);
            //OUTPUT
            //2 (&& 3)
            outputNInterativo(matrizLeslie, nElementos, geracao, dimensao, ficheiros[1],argumentos(args), ficheiros);
            //4
            graficosNInterativo(args);
        } else {
            System.out.println("-> Introduza valores possiveis para o numero da faixa etária e para o " +
                    "numero de gerações");
            System.exit(0);
        }
    }

    public static String nomeFicheiroEntrada(String fEntrada) {
        String[] nomeEspecie = fEntrada.split("\\.");
        return nomeEspecie[0];
    }

    //----------------------------------------------------PONTO1--------------------------------------------------------
    //Métodos modo interativo sem ficheiro
    public static double[][] criarArrayNumElementos(int dim) {
        double[][] nElementos = new double[dim][1];
        System.out.println("-> No caso de querer sair/reiniciar a introdução dos valores por favor insira o valor -1");
        System.out.println("-> No caso de querer introduzir um ficheiro pessoal insira o valor -2");
        for (int i = 0; i < dim; i++) {
            System.out.println("-> Por favor introduza o número de elementos da faixa etária " + (i + 1) + ":");
            double elementos = in.nextDouble();
            if (elementos >= 0) {
                nElementos[i][0] = elementos;
            } else if(elementos == -1){
                i = -1;
                System.out.println("-> Por favor coloque os valores de novo");
            } else if(elementos == -2){
                nElementos[0][0] = -2;
                return nElementos;
            }
        }
        return nElementos;
    }

    public static double[][] criarMatrizLeslie(int dim) {
        double[][] matrizLeslie = new double[dim][dim];
        System.out.println("-> No caso de querer sair/reiniciar a introdução dos valores por favor insira o valor -1");
        System.out.println("-> No caso de querer introduzir um ficheiro pessoal insira o valor -2");
        for (int j = 0; j < dim; j++) {
            System.out.println("-> Por favor introduza as taxas de fecundidade na faixa etária " + (j + 1) + ":");
            double taxaFecundidade = in.nextDouble();
            if (taxaFecundidade >= 0) {
                matrizLeslie[0][j] = taxaFecundidade;
            } else if(taxaFecundidade == -1){
                j = -1;
                System.out.println("-> Por favor coloque os valores de novo");
            } else if(taxaFecundidade == -2){
                matrizLeslie[0][0] = -2;
                return matrizLeslie;
            }
        }
        if(matrizLeslie[0][0] != -2) {
            System.out.println("-> No caso de querer sair/reiniciar a introdução dos valores por favor insira " +
                    "o valor -1");
            System.out.println("-> No caso de querer introduzir um ficheiro pessoal insira o valor -2");
            for (int i = 1; i < dim; i++) {
                System.out.println("-> Por favor introduza as taxas de sobrevivência na faixa etária " + i + ":");
                int j = i - 1;
                double taxasobrevivencia = in.nextDouble();
                if (taxasobrevivencia <= 1 && taxasobrevivencia >= 0) {
                    matrizLeslie[i][j] = taxasobrevivencia;
                } else if (taxasobrevivencia == -1) {
                    i = 0;
                    System.out.println("-> Por favor coloque os valores de novo");
                } else if (taxasobrevivencia == -2){
                    matrizLeslie[0][0] = -2;
                    return matrizLeslie;
                } else{
                    System.out.println("-> Valor não permitido");
                    i -= 1;
                }
            }
        }
        return matrizLeslie;
    }

    //Métodos modo interativo(I)/não interativo(NI) com ficheiro
    public static int dimensao(String nome) throws FileNotFoundException {
        int cont = 0;
        Scanner readFicheiro = new Scanner(new File(nome));
        while (readFicheiro.hasNextLine()) {
            String linhaFicheiro = readFicheiro.nextLine();
            for (int i = 0; i < linhaFicheiro.length(); i++) {
                if (String.valueOf(linhaFicheiro.charAt(i)).equals("x")) {
                    cont++;
                }
            }
        }
        return cont;
    }

    public static double[][] criarArrayNumElementosFile(String fEntrada, int dimensao) throws FileNotFoundException {
        int cont = 0, contador = 0, numConfirmar;
        String confirmar;
        String[] nElementos;
        double[][] numElementos = new double[dimensao][1];
        Scanner readFicheiro = new Scanner(new File(fEntrada));
        String linha = null;
        while (readFicheiro.hasNextLine()) {
            String linhaFicheiro = readFicheiro.nextLine();
            for (int i = 0; i < linhaFicheiro.length(); i++) {
                if (String.valueOf(linhaFicheiro.charAt(i)).equals("x")) {
                    confirmar = "";
                    for(int j = i + 1; j < i + 3; j ++){
                        confirmar += String.valueOf(linhaFicheiro.charAt(j));
                    }
                    numConfirmar = Integer.parseInt(confirmar);
                    if(numConfirmar != contador){
                        System.out.println("-> O ficheiro inserido pelo utilizador possui erros na sua estrutura.");
                        System.out.println("-> Erro detetado na linha do número de elementos.");
                        System.out.println("-> Modifique o ficheiro, ou insira outro.");
                        System.exit(0);
                    }
                    else{
                        contador++;
                    }
                }
            }
            for (int i = 0; i < linhaFicheiro.length(); i++) {
                if (String.valueOf(linhaFicheiro.charAt(i)).equals("x")) {
                    linha = linhaFicheiro;
                    break;
                }
            }
        }
        linha = linha.replace("=", ",");
        nElementos = linha.split(",");
        for (int i = 1; i < nElementos.length; i = i + 2) {
            if (Double.parseDouble(nElementos[i]) >= 0) {
                numElementos[cont][0] = Double.parseDouble(nElementos[i]);
                cont++;
            } else {
                System.out.println("-> Valor inválido. Por favor insira outros valores no ficheiro.");
                System.exit(0);
            }
        }
        return numElementos;
    }

    public static double[][] criarMatrizLeslieFile(String fEntrada, int dimensao) throws FileNotFoundException {
        int cont = 0, contadorS = 0, contadorF = 0, numConfirmar;
        String linha = null, linha2 = null, confirmar;
        double[][] matrizLesleiFile = new double[dimensao][dimensao];
        String[] taxaSob, taxaFec;
        double[] taxaSobrevivencia = new double[dimensao - 1];
        double[] taxaFecundidade = new double[dimensao];
        Scanner readFicheiro = new Scanner(new File(fEntrada));
        while (readFicheiro.hasNextLine()) {
            String linhaFicheiro = readFicheiro.nextLine();
            for (int i = 0; i < linhaFicheiro.length(); i++) {
                if (String.valueOf(linhaFicheiro.charAt(i)).equals("s")) {
                    confirmar = "";
                    if(contadorS < 10){
                        int j = i + 1;
                        confirmar = String.valueOf(linhaFicheiro.charAt(j));
                    }
                    else{
                        for(int j = i + 1; j < i + 3; j ++){
                            confirmar += String.valueOf(linhaFicheiro.charAt(j));
                        }
                    }
                    numConfirmar = Integer.parseInt(confirmar);
                    if(numConfirmar != contadorS){
                        System.out.println("-> O ficheiro inserido pelo utilizador possui erros na sua estrutura.");
                        System.out.println("-> Erro detetado na linha da taxa de sobrevivência.");
                        System.out.println("-> Modifique o ficheiro, ou insira outro.");
                        System.exit(0);
                    }
                    else{
                        contadorS++;
                    }
                } else if (String.valueOf(linhaFicheiro.charAt(i)).equals("f")) {
                    confirmar = "";
                    if(contadorF < 10){
                        int j = i + 1;
                        confirmar = String.valueOf(linhaFicheiro.charAt(j));
                    }
                    else{
                        for(int j = i + 1; j < i + 3; j ++){
                            confirmar += String.valueOf(linhaFicheiro.charAt(j));
                        }
                    }
                    numConfirmar = Integer.parseInt(confirmar);
                    if(numConfirmar != contadorF){
                        System.out.println("-> O ficheiro inserido pelo utilizador possui erros na sua estrutura.");
                        System.out.println("-> Erro detetado na linha da taxa de fecundidade.");
                        System.out.println("-> Modifique o ficheiro, ou insira outro.");
                        System.exit(0);
                    }
                    else{
                        contadorF++;
                    }
                }
            }
            for (int i = 0; i < linhaFicheiro.length(); i++) {
                if (String.valueOf(linhaFicheiro.charAt(i)).equals("s")) {
                    linha = linhaFicheiro;
                } else if (String.valueOf(linhaFicheiro.charAt(i)).equals("f")) {
                    linha2 = linhaFicheiro;
                }
            }
        }
        linha = linha.replace("=", ",");
        taxaSob = linha.split(",");
        for (int i = 1; i < taxaSob.length; i = i + 2) {
            taxaSobrevivencia[cont] = Double.parseDouble(taxaSob[i]);
            cont++;
        }
        cont = 0;
        linha2 = linha2.replace("=", ",");
        taxaFec = linha2.split(",");
        for (int i = 1; i < taxaFec.length; i = i + 2) {
            taxaFecundidade[cont] = Double.parseDouble(taxaFec[i]);
            cont++;
        }
        for (int i = 0; i < matrizLesleiFile.length; i++) {
            matrizLesleiFile[0][i] = taxaFecundidade[i];
        }
        for (int i = 1; i < matrizLesleiFile.length; i++) {
            int j = i - 1;
            matrizLesleiFile[i][j] = taxaSobrevivencia[j];
        }
        return matrizLesleiFile;
    }

    //----------------------------------------------------PONTO2--------------------------------------------------------
    public static double multiplicarMatrizMembroAMembro(double[][] primeiraMatriz, double[][] segundaMatriz,
                                                        int linha, int coluna) {
        double celula = 0;
        for (int i = 0; i < segundaMatriz.length; i++) {
            celula += primeiraMatriz[linha][i] * segundaMatriz[i][coluna];
        }
        return celula;
    }

    public static double[][] multiplicarMatrizes(double[][] primeiraMatriz, double[][] segundaMatriz) {
        double[][] resultado = new double[primeiraMatriz.length][segundaMatriz[0].length];
        for (int i = 0; i < resultado.length; i++) {
            for (int j = 0; j < resultado[i].length; j++) {
                resultado[i][j] = multiplicarMatrizMembroAMembro(primeiraMatriz, segundaMatriz, i, j);
            }
        }
        return resultado;
    }

    //----------------------------------------------------PONTO3--------------------------------------------------------
    public static double evolucaoPopulacao(double[][] distribuicao, int dim) {
        double somatorioN = 0;
        for (int i = 0; i < dim; i++) {
            somatorioN += distribuicao[i][0];
        }
        return somatorioN;
    }

    public static double taxaVariacao(double[][] distribuicao, double[][] matrizLeslie, int dim) {
        double taxaVariacao,distribuicao1,distribuicao2;
        distribuicao1 = evolucaoPopulacao(distribuicao, dim);
        distribuicao = multiplicarMatrizes(matrizLeslie, distribuicao);
        distribuicao2 = evolucaoPopulacao(distribuicao, dim);
        taxaVariacao = (distribuicao2/ distribuicao1);
        return taxaVariacao;
    }

    //----------------------------------------------------PONTO 2/3-----------------------------------------------------
    public static double[][] matrizIdentidade(int dimensao) {
        double[][] matrizIdent = new double[dimensao][dimensao];
        for (int i = 0; i < dimensao; i++) {
            for (int j = 0; j < dimensao; j++) {
                if (i == j) {
                    matrizIdent[i][j] = 1;
                } else {
                    matrizIdent[i][j] = 0;
                }
            }
        }
        return matrizIdent;
    }

    public static double[][] distribuicaoDaPopulacao(double[][] matrizLeslie, double[][] nElementos, int gera,
                                                     int dimensao) {
        double[][] distribuicao,multiplicacaoMatrizLeslie;
        multiplicacaoMatrizLeslie = matrizIdentidade(dimensao);
        for (int g = 0; g < gera; g++) {
            multiplicacaoMatrizLeslie = multiplicarMatrizes(multiplicacaoMatrizLeslie, matrizLeslie);
        }
        distribuicao = multiplicarMatrizes(multiplicacaoMatrizLeslie, nElementos);
        return distribuicao;
    }

    public static double[][] distribuicaoNormalizadaDaPopulacao(double[][] distribuicao, int dimensao) {
        double[][] normalizada = new double[dimensao][1];
        double dimPopulacao= evolucaoPopulacao(distribuicao, dimensao);
        for (int i = 0; i < dimensao; i++) {
            normalizada[i][0] = (distribuicao[i][0] / dimPopulacao)*100;
        }
        return normalizada;
    }

    //--------------------------------------------------------PONTO4----------------------------------------------------
    public static double maximoValorProprio(double[][] matrizleslie) {
        Matrix matrizes = new Basic2DMatrix(matrizleslie);
        //Obtem valores e vetores próprios fazendo "Eigen Decomposition"
        EigenDecompositor eigenD = new EigenDecompositor(matrizes);
        Matrix[] mattD = eigenD.decompose();
        // converte objeto Matrix  para array Java
        double[][] matrizValores = mattD[1].toDenseMatrix().toArray();
        int j = matrizValores.length - 1;
        double dominante = -1;
        for (int i = 0; i < matrizValores.length; i++) {
            if (matrizValores[i][i] > 0 && matrizValores[i][i] > dominante) {
                if (i == 0 && matrizValores[i][i + 1] == 0) {
                    dominante = matrizValores[i][i];
                }
                if (i == j && matrizValores[i][i - 1] == 0) {
                    dominante = matrizValores[i][i];
                }
                if (i != 0 && i != j && matrizValores[i][i - 1] == 0 && matrizValores[i][i + 1] == 0) {
                    dominante = matrizValores[i][i];
                }
            }
        }
        return dominante;
    }
    public static double[] vetorProprio(int dimensao, double maxValor, double[][] matrizleslie){
        Matrix a = new Basic2DMatrix(matrizleslie);
        int cont = 0;
        double soma=0;
        double[] vetor=new double[dimensao];
        EigenDecompositor eigenD = new EigenDecompositor(a);
        Matrix[] matrizes = eigenD.decompose();
        double[][] matrizValores = matrizes[1].toDenseMatrix().toArray();
        double[][] matrizVetores=matrizes[0].toDenseMatrix().toArray();
        for (double[] doubles : matrizValores) {
            for (int j = 0; j < matrizValores.length; j++) {
                if (maxValor == doubles[j]) {
                    cont = j;
                }
            }
        }
        for (int l=0;l<matrizValores.length;l++){
            vetor[l]=matrizVetores[l][cont];
            soma+=vetor[l];
        }
        double[] vetorProprio=new double[dimensao];
        for(int o=0;o<matrizValores.length;o++){
            vetorProprio[o]=(vetor[o]/soma)*100;
        }
        return vetorProprio;
    }

    //----------------------------------------------------PONTO5--------------------------------------------------------
    public static void outputInterativo(double[][] matrizLeslie, double[][] nElementos,
                                        int gera, int dimensao, String[] args) throws IOException {
        Locale.setDefault(Locale.US);
        String nomeEspecie = null;
        double[][] distribuicaoP;
        double[] vetor;
        double dimensaoPopulacao, taxaVariacao, valor;
        valor = maximoValorProprio(matrizLeslie);
        vetor= vetorProprio(dimensao, valor, matrizLeslie);
        if (args.length == 0) {
            System.out.println("-> Mencione o nome da espécie:");
            nomeEspecie = in.next();
        } else if (args.length == 2) {
            nomeEspecie = nomeFicheiroEntrada(args[1]);
        }
        System.out.println("k="+gera);
        System.out.println("-> Matriz de Leslie");
        for (int i=0;i<dimensao;i++){
            System.out.print("(");
            for(int j=0;j<dimensao;j++){
                if( j+1<dimensao) {
                    System.out.printf("%.2f, ", matrizLeslie[i][j]);
                }else{
                    System.out.printf("%.2f", matrizLeslie[i][j]);
                }
            }
            System.out.println(")");
        }
        for (int g = 0; g <= gera; g++) {
            distribuicaoP = distribuicaoDaPopulacao(matrizLeslie, nElementos, g, dimensao);
            dimensaoPopulacao = evolucaoPopulacao(distribuicaoP, dimensao);
            taxaVariacao = taxaVariacao(distribuicaoP, matrizLeslie, dimensao);
            System.out.println("-> A distribuição (não normalizada) da população da geração " + g + " é: ");
            double[][] evolucaoP = distribuicaoDaPopulacao(matrizLeslie, nElementos, g, dimensao);
            for (int i = 1; i <= dimensao; i++) {
                System.out.printf("FT"+i+": "+"%.2f%n",evolucaoP[i - 1][0]);
            }
            System.out.println("-------------------------------------------------------------------------------------");
            System.out.print("-> A dimensão da população na geração " + g + " é: ");
            System.out.printf("%.2f%n",dimensaoPopulacao);
            System.out.println("-------------------------------------------------------------------------------------");
            System.out.print("-> A taxa de variação da população da geracão "+g+" é: ");
            System.out.printf("%.2f%n", taxaVariacao);
            System.out.println("-------------------------------------------------------------------------------------");
            double [][] normalizada = distribuicaoNormalizadaDaPopulacao(evolucaoP, dimensao);
            System.out.println("-> A distribuição normalizada da população na geração " + g + " é:");
            for (int i = 1; i <= dimensao; i++) {
                System.out.printf("FT"+i+": "+"%.2f%n",normalizada[i - 1][0]);
            }
            System.out.println("-------------------------------------------------------------------------------------");
        }
        System.out.printf("-> Valor próprio: "+"delta: "+"%.4f%n", valor);
        System.out.print("-> Vetor próprio: ");
        System.out.print("(");
        for(int i=0; i<dimensao; i++){
            if(i+1<dimensao) {
                System.out.printf("%.2f, ", vetor[i]);
            }else{
                System.out.printf("%.2f", vetor[i]);
            }
        }
        System.out.println(")");
        criarFicheiros(nomeEspecie, matrizLeslie, nElementos, gera, dimensao);
    }

    public static void graficosInterativo() throws IOException {
        Runtime rt = Runtime.getRuntime();
        System.out.println("-> Deseja visualizar o gráfico da Evolução da espécie? Responda com (Sim/Não)");
        String resposta = in.next();
        while (!resposta.equalsIgnoreCase("Sim") && !resposta.equalsIgnoreCase("Yes")
                && !resposta.equalsIgnoreCase("S") && !resposta.equalsIgnoreCase("Y") &&
                !resposta.equalsIgnoreCase("Não") && !resposta.equalsIgnoreCase("No")
                && !resposta.equalsIgnoreCase("N") && !resposta.equalsIgnoreCase("Nao")) {
            System.out.println("-> Resposta inválida, responda novamente");
            resposta = in.next();
        }
        if (resposta.equalsIgnoreCase("Sim") || resposta.equalsIgnoreCase("Yes")
                || resposta.equalsIgnoreCase("S") || resposta.equalsIgnoreCase("Y")) {
            Process prcs = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe -p scripts\\evolucaoqt");
        }
        System.out.println("-> Deseja guardar o gráfico da Evolução da espécie? Responda com (Sim/Não)");
        resposta = in.next();
        while (!resposta.equalsIgnoreCase("Sim") && !resposta.equalsIgnoreCase("Yes")
                && !resposta.equalsIgnoreCase("S") && !resposta.equalsIgnoreCase("Y") &&
                !resposta.equalsIgnoreCase("Não") && !resposta.equalsIgnoreCase("No")
                && !resposta.equalsIgnoreCase("N") && !resposta.equalsIgnoreCase("Nao")) {
            System.out.println("-> Resposta inválida, responda novamente");
            resposta = in.next();
        }
        if (resposta.equalsIgnoreCase("Sim") || resposta.equalsIgnoreCase("Yes")
                || resposta.equalsIgnoreCase("S") || resposta.equalsIgnoreCase("Y")) {
            System.out.println("-> Qual é o formato que deseja gravar o gráfico?");
            System.out.println("(1=png 2=txt 3=eps)");
            int formato = in.nextInt();
            if (formato == 1) {
                Process prcs1 = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                        "-p scripts\\evolucaopng");
            }
            if (formato == 2) {
                Process prcs1 = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                        "-p scripts\\evolucaotxt");
            }
            if (formato == 3) {
                Process prcs1 = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                        "-p scripts\\evolucaoeps");
            }
        }
        System.out.println("-> Deseja visualizar o gráfico da Distibuição Normalizada da espécie? " +
                "Responda com (Sim/Não)");
        resposta = in.next();
        while (!resposta.equalsIgnoreCase("Sim") && !resposta.equalsIgnoreCase("Yes")
                && !resposta.equalsIgnoreCase("S") && !resposta.equalsIgnoreCase("Y") &&
                !resposta.equalsIgnoreCase("Não") && !resposta.equalsIgnoreCase("No")
                && !resposta.equalsIgnoreCase("N") && !resposta.equalsIgnoreCase("Nao")) {
            System.out.println("-> Resposta inválida, responda novamente");
            resposta = in.next();
        }
        if (resposta.equalsIgnoreCase("Sim") || resposta.equalsIgnoreCase("Yes")
                || resposta.equalsIgnoreCase("S") || resposta.equalsIgnoreCase("Y")) {
            Process prcs = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe -p scripts\\normalizadaqt");
        }
        System.out.println("-> Deseja guardar o gráfico da Distibuição Normalizada da espécie? Responda com (Sim/Não)");
        resposta = in.next();
        while (!resposta.equalsIgnoreCase("Sim") && !resposta.equalsIgnoreCase("Yes")
                && !resposta.equalsIgnoreCase("S") && !resposta.equalsIgnoreCase("Y") &&
                !resposta.equalsIgnoreCase("Não") && !resposta.equalsIgnoreCase("No")
                && !resposta.equalsIgnoreCase("N") && !resposta.equalsIgnoreCase("Nao")) {
            System.out.println("-> Resposta inválida, responda novamente");
            resposta = in.next();
        }
        if (resposta.equalsIgnoreCase("Sim") || resposta.equalsIgnoreCase("Yes")
                || resposta.equalsIgnoreCase("S") || resposta.equalsIgnoreCase("Y")) {
            System.out.println("Qual é o formato que deseja gravar o gráfico?");
            System.out.println("(1=png 2=txt 3=eps)");
            int formato = in.nextInt();
            if (formato == 1) {
                Process prcs1 = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                        "-p scripts\\normalizadapng");
            }
            if (formato == 2) {
                Process prcs1 = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                        "-p scripts\\normalizadatxt");
            }
            if (formato == 3) {
                Process prcs1 = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                        "-p scripts\\normalizadaeps");
            }
        }
        System.out.println("-> Deseja visualizar o gráfico da Distribuição (não Normalizada) da espécie?" +
                " Responda com (Sim/Não)");
        resposta = in.next();
        while (!resposta.equalsIgnoreCase("Sim") && !resposta.equalsIgnoreCase("Yes")
                && !resposta.equalsIgnoreCase("S") && !resposta.equalsIgnoreCase("Y") &&
                !resposta.equalsIgnoreCase("Não") && !resposta.equalsIgnoreCase("No")
                && !resposta.equalsIgnoreCase("N") && !resposta.equalsIgnoreCase("Nao")) {
            System.out.println("-> Resposta inválida, responda novamente");
            resposta = in.next();
        }
        if (resposta.equalsIgnoreCase("Sim") || resposta.equalsIgnoreCase("Yes")
                || resposta.equalsIgnoreCase("S") || resposta.equalsIgnoreCase("Y")) {
            Process prcs = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                    "-p scripts\\naoNormalizadaqt");
        }
        System.out.println("-> Deseja guardar o gráfico da Distribuição (não Normalizada) da espécie?" +
                " Responda com (Sim/Não)");
        resposta = in.next();
        while (!resposta.equalsIgnoreCase("Sim") && !resposta.equalsIgnoreCase("Yes")
                && !resposta.equalsIgnoreCase("S") && !resposta.equalsIgnoreCase("Y") &&
                !resposta.equalsIgnoreCase("Não") && !resposta.equalsIgnoreCase("No")
                && !resposta.equalsIgnoreCase("N") && !resposta.equalsIgnoreCase("Nao")) {
            System.out.println("-> Resposta inválida, responda novamente");
            resposta = in.next();
        }
        if (resposta.equalsIgnoreCase("Sim") || resposta.equalsIgnoreCase("Yes")
                || resposta.equalsIgnoreCase("S") || resposta.equalsIgnoreCase("Y")) {
            System.out.println("-> Qual é o formato que deseja gravar o gráfico?");
            System.out.println("(1=png 2=txt 3=eps)");
            int formato = in.nextInt();
            if (formato == 1) {
                Process prcs1 = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                        "-p scripts\\naoNormalizadapng");
            }
            if (formato == 2) {
                Process prcs1 = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                        "-p scripts\\naoNormalizadatxt");
            }
            if (formato == 3) {
                Process prcs1 = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                        "-p scripts\\naoNormalizadaeps");
            }
        }
        System.out.println("-> Deseja visualizar o gráfico da Taxa de Variação da espécie? Responda com (Sim/Não)");
        resposta = in.next();
        while (!resposta.equalsIgnoreCase("Sim") && !resposta.equalsIgnoreCase("Yes")
                && !resposta.equalsIgnoreCase("S") && !resposta.equalsIgnoreCase("Y") &&
                !resposta.equalsIgnoreCase("Não") && !resposta.equalsIgnoreCase("No")
                && !resposta.equalsIgnoreCase("N") && !resposta.equalsIgnoreCase("Nao")) {
            System.out.println("-> Resposta inválida, responda novamente");
            resposta = in.next();
        }
        if (resposta.equalsIgnoreCase("Sim") || resposta.equalsIgnoreCase("Yes")
                || resposta.equalsIgnoreCase("S") || resposta.equalsIgnoreCase("Y")) {
            Process prcs = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe -p scripts\\taxaVariacaoqt");
        }
        System.out.println("-> Deseja guardar o gráfico da Taxa de Variação da espécie? Responda com (Sim/Não)");
        resposta = in.next();
        while (!resposta.equalsIgnoreCase("Sim") && !resposta.equalsIgnoreCase("Yes")
                && !resposta.equalsIgnoreCase("S") && !resposta.equalsIgnoreCase("Y") &&
                !resposta.equalsIgnoreCase("Não") && !resposta.equalsIgnoreCase("No")
                && !resposta.equalsIgnoreCase("N") && !resposta.equalsIgnoreCase("Nao")) {
            System.out.println("-> Resposta inválida, responda novamente");
            resposta = in.next();
        }
        if (resposta.equalsIgnoreCase("Sim") || resposta.equalsIgnoreCase("Yes")
                || resposta.equalsIgnoreCase("S") || resposta.equalsIgnoreCase("Y")) {
            System.out.println("-> Qual é o formato que deseja gravar o gráfico?");
            System.out.println("(1=png 2=txt 3=eps)");
            int formato = in.nextInt();
            if (formato == 1) {
                Process prcs1 = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                        "-p scripts\\taxaVariacaopng");
            }
            if (formato == 2) {
                Process prcs1 = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                        "-p scripts\\taxaVariacaotxt");
            }
            if (formato == 3) {
                Process prcs1 = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                        "-p scripts\\taxaVariacaoeps");
            }
        }
    }

    //----------------------------------------------------PONTO6--------------------------------------------------------
    public static int geracoesNI(String[] args) {
        int geracao = 0;
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-t")) {
                geracao = parseInt(args[i + 1]);
            }
        }
        return geracao;
    }

    public static String tipoDeFicheiroNI(String[] args){
        String tipoDeFich = null;
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-g")) {
                tipoDeFich = args[i + 1];
            }
        }
        return tipoDeFich;
    }

    public static int[] argumentos(String[] args) {
        int[] argumentos = new int[3];
        for (String arg : args) {
            if (arg.equals("-v")) {
                argumentos[0] = 1;
            }
            if (arg.equals("-r")) {
                argumentos[1] = 1;
            }
            if (arg.equals("-e")) {
                argumentos[2] = 1;
            }
        }
        return argumentos;
    }

    public static void outputNInterativo(double[][] matrizLeslie, double[][] nElementos, int gera, int dimensao
            , String fsaida, int[] argumentos, String[] ficheiros) throws IOException {
        Locale.setDefault(Locale.US);
        double[][] normalizada, distribuicaoP;
        double[] vetor;
        double dimensaoPopulacao, taxaVariacao, valor;
        String nomeEspecie = nomeFicheiroEntrada(ficheiros[0]);
        criarFicheiros(nomeEspecie, matrizLeslie, nElementos, gera, dimensao);
        PrintWriter write = new PrintWriter(fsaida);
        valor = maximoValorProprio(matrizLeslie);
        vetor = vetorProprio(dimensao, valor, matrizLeslie);
        write.println("k=" + gera);
        write.println("-> Matriz de Leslie");
        for (int i = 0; i < dimensao; i++) {
            write.printf("(");
            for (int j = 0; j < dimensao; j++) {
                if (j + 1 < dimensao) {
                    write.printf("%.2f, ", matrizLeslie[i][j]);
                } else {
                    write.printf("%.2f", matrizLeslie[i][j]);
                }
            }
            write.println(")");
        }
        for (int g = 0; g <= gera; g++) {
            distribuicaoP = distribuicaoDaPopulacao(matrizLeslie, nElementos, g, dimensao);
            normalizada = distribuicaoNormalizadaDaPopulacao(distribuicaoP, dimensao);
            dimensaoPopulacao = evolucaoPopulacao(distribuicaoP, dimensao);
            taxaVariacao = taxaVariacao(distribuicaoP, matrizLeslie, dimensao);
            write.println("---------------------------------------------------------------------");
            write.println("-> A distribuição (não normalizada) da população da geração " + g + " é: ");
            for (int i = 1; i <= dimensao; i++) {
                write.print("FT" + i + ": ");
                write.printf("%.2f%n", distribuicaoP[i - 1][0]);
            }
            write.println("---------------------------------------------------------------------");
            write.println("-> A distribuição normalizada da população da geração " + g + " é: ");
            for (int i = 1; i <= dimensao; i++) {
                write.print("FT" + i + ": ");
                write.printf("%.2f%n", normalizada[i - 1][0]);
            }
            if (argumentos[0] == 1) {
                write.println("---------------------------------------------------------------------");
                write.print("-> A dimensão da população da geracão " + g + " é: ");
                write.printf("%.2f%n", dimensaoPopulacao);
            }
            if (argumentos[1] == 1) {
                write.println("---------------------------------------------------------------------");
                write.print("-> A taxa de variação da população da geracão " + g + " é: ");
                write.printf("%.2f%n", taxaVariacao);
            }
        }
        if (argumentos[2] == 1) {
            write.println("---------------------------------------------------------------------");
            write.printf("-> Valor próprio: " + "delta: %.4f%n", valor);
            write.print("-> Vetor próprio: ");
            write.print("(");
            for (int i = 0; i < dimensao; i++) {
                if (i + 1 < dimensao) {
                    write.printf("%.2f, ", vetor[i]);
                } else {
                    write.printf("%.2f", vetor[i]);
                }
            }
            write.println(")");
        }
        write.println("---------------------------------------------------------------------");
        write.close();
    }

    public static void graficosNInterativo(String[] args) throws IOException {
        Runtime rt = Runtime.getRuntime();
        int[] argumento=argumentos(args);
        switch (tipoDeFicheiroNI(args)){
            case "1": {
                Process prcsN = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                        "-p scripts\\normalizadapng");
                Process prcsE = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                        "-p scripts\\naoNormalizadapng");
                if (argumento[0] == 1) {
                    Process prcsD = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                            "-p scripts\\evolucaopng");
                }
                if (argumento[1] == 1) {
                    Process prcsT = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                            "-p scripts\\taxaVariacaopng");
                }
                break;
            }
            case "2": {
                Process prcsN = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                        "-p scripts\\normalizadatxt");
                Process prcsE = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                        "-p scripts\\naoNormalizadatxt");
                if (argumento[0] == 1) {
                    Process prcsD = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                            "-p scripts\\evolucaotxt");
                }
                if (argumento[1] == 1) {
                    Process prcsT = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                            "-p scripts\\taxaVariacaotxt");
                }
                break;
            }
            case "3": {
                Process prcsN = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                        "-p scripts\\normalizadaeps");
                Process prcsE = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                        "-p scripts\\naoNormalizadaeps");
                if (argumento[0] == 1) {
                    Process prcsD = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                            "-p scripts\\evolucaoeps");
                }
                if (argumento[1] == 1) {
                    Process prcsT = rt.exec("C:\\Program Files\\gnuplot\\bin\\wgnuplot.exe " +
                            "-p scripts\\taxaVariacaoeps");
                }
                break;
            }
        }
    }

    //----------------------------------------------------PONTO5/6------------------------------------------------------
    public static void criarFicheiros(String nomeEspecie,double[][] matrizLeslie, double[][] nElementos,
                                      int gera, int dimensao) throws IOException {
        double[][] normalizada, distribuicaoP;
        double taxaVariacao, dimensaoPopulacao;
        PrintWriter writeE = new PrintWriter("texts\\naoNormalizada.txt");
        PrintWriter writeN = new PrintWriter("texts\\normalizada.txt");
        PrintWriter writeD = new PrintWriter("texts\\evolucao.txt");
        PrintWriter writeT = new PrintWriter("texts\\taxaVariacao.txt");
        writeE.println(nomeEspecie);
        writeN.println(nomeEspecie);
        writeD.println(nomeEspecie);
        writeT.println(nomeEspecie);
        for (int g = 0; g <= gera; g++) {
            distribuicaoP = distribuicaoDaPopulacao(matrizLeslie, nElementos, g, dimensao);
            taxaVariacao = taxaVariacao(distribuicaoP, matrizLeslie, dimensao);
            dimensaoPopulacao = evolucaoPopulacao(distribuicaoP, dimensao);
            writeE.print(g + " ");
            writeN.print(g + " ");
            writeD.println(g + " " + dimensaoPopulacao);
            writeT.println(g + " " + taxaVariacao);
            normalizada = distribuicaoNormalizadaDaPopulacao(distribuicaoP, dimensao);
            for (int i = 1; i <= dimensao; i++) {
                writeE.print(distribuicaoP[i - 1][0] + " ");
                writeN.print(normalizada[i - 1][0] + " ");
            }
            writeE.println("");
            writeN.println("");
        }
        writeE.close();
        writeN.close();
        writeD.close();
        writeT.close();
    }
}